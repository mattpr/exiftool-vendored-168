const gulp       = require('gulp'),
    glob = require('glob'),
    fs = require('node:fs'),
    path = require('node:path');

const srcDir = 'src/files';

async function stripMetaTest () {
    const rePDF = /\.pdf$/i,
        ExifTool = require('exiftool-vendored').ExifTool,
        exiftool = new ExifTool({ taskTimeoutMillis: 5000 }),
        files = glob.sync(srcDir+'/*', {
            nodir: true,
        });

    for (const inFile of files) {

        if (rePDF.exec(inFile)) { // looks like a pdf
            try {
                let etvResult = await exiftool.write(inFile, {
                    Title: null,
                    Author: null,
                    Subject: null,
                    Creator: null,
                    Producer: null,
                    Keywords: null,
                }, ['-overwrite_original_in_place']);
                console.log(`exiftool-vendored ${inFile} reports:`, etvResult)
            }
            catch (e) {
                console.error('exiftool-vendored threw an error:', e);
                exiftool.end();
                throw e;
            }
        }
    }
    await exiftool.end();

}


module.exports.build = stripMetaTest;


module.exports = {
    root: true,
    extends: [
        'eslint:recommended',
    ],
    env: {
        node: true,
        es6: true,
    },
    parserOptions: {
        
        sourceType: 'module',
        ecmaFeatures: {
            modules: true
        }
    },
    rules: {
        
    }
};
